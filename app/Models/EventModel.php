<?php

namespace App\Models;

use CodeIgniter\Model;

class EventModel extends Model
{
  protected $table = "Event";
  protected $useTimestamp = true;

  public function getEvent($slug = false)
  {
    if ($slug == false) {
      return $this->findAll();
    }

    return $this->where(["slug" => $slug])->first();
  }
}