<?php

namespace App\Controllers;

class Pages extends BaseController
{
  public function index()
  {
    $data = [
      "title" => "Home | HIMATEKKOM",
      "devisi" => ["HUMAS", "DANUS", "PSDM"],
    ];

    return view("pages/home", $data);
  }

  public function about()
  {
    $data = [
      "title" => "About | HIMATEKKOM",
    ];

    return view("pages/about", $data);
  }

  public function contact()
  {
    $data = [
      "title" => "Contact | HIMATEKKOM",
      "alamat" => [
        [
          "tipe" => "Rumah",
          "alamat" => "Jl.Kesatria Dusun IX",
          "kota" => "Tanjung Beringin",
        ],
        [
          "tipe" => "Kantor",
          "alamat" => "Jl. SetiaBudi No.65",
          "kota" => "Medan",
        ],
      ],
    ];

    return view("pages/contact", $data);
  }
}