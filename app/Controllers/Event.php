<?php

namespace App\Controllers;

use App\Models\EventModel;

class Event extends BaseController
{
  protected $eventModel;
  public function __construct()
  {
    $this->eventModel = new EventModel();
  }

  public function index()
  {
    // $event = $this->eventModel->findAll();

    $data = [
      "title" => "Daftar | Event",
      "event" => $this->eventModel->getEvent(),
    ];

    // Cara Connect db Tanpa Model
    // $db = \Config\Database::connect();
    // $event = $db->query("SELECT * FROM Event");
    // foreach ($event->getResultArray() as $row) {
    //   d($row);
    // }

    // $eventModel = new \App\Models\EventModel();
    // $eventModel = new EventModel();

    return view("event/index", $data);
  }

  public function detail($slug)
  {
    // $event = $this->eventModel->getEvent($slug);
    $data = [
      "title" => "Detail Event",
      "event" => $this->eventModel->getEvent($slug),
    ];
    return view("event/detail", $data);
  }
}