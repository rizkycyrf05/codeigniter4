<?= $this->extend("layout/template") ?>

<?= $this->section("content") ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h1 class="mt-2">Daftar Event</h1>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Gambar</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($event as $e): ?>
                    <tr>
                        <th scope="row"><?= $e["id"] ?></th>
                        <td><img src="/img/<?= $e[
                          "gambar"
                        ] ?>" alt="Biner4.0" class="gambar"></td>
                        <td><?= $e["judul"] ?></td>
                        <td><a href="/event/<?= $e[
                          "slug"
                        ] ?>" class="btn btn-primary">Detail</a></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?= $this->endSection() ?>