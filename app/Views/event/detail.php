<?= $this->extend("layout/template") ?>


<?= $this->section("content") ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h2 class="mt-2">Detail Komik</h2>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img src="/img/<?= $event[
                          "gambar"
                        ] ?>" class="img-fluid rounded-start" alt="<?= $event[
  "slug"
] ?>">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= $event["judul"] ?></h5>
                            <p class="card-text"><b>Pelaksana : </b><?= $event[
                              "pelaksana"
                            ] ?></p>
                            <p class="card-text"><small class="text-muted"><b>Divisi : </b><?= $event[
                              "devisi"
                            ] ?></small></p>
                            <a href="" class="btn btn-success">Edit</a>
                            <a href="" class="btn btn-danger">Delete</a>
                            <br>
                            <a href="/event">Kembali Ke Daftar Event</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>